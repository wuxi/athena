/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtSensitiveDetectorTool.h"
#include "../RpcSensitiveDetectorTool.h"

DECLARE_COMPONENT( MuonG4R4::MdtSensitiveDetectorTool )
DECLARE_COMPONENT( MuonG4R4::RpcSensitiveDetectorTool )
